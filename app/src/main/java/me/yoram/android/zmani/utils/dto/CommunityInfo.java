/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.utils.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 11/03/18
 */
public class CommunityInfo {
    /**
     * Unique community identifier
     */
    private String id;

    /**
     * Community name
     */
    private String name;

    /**
     * Program list
     */
    private List<Program> programs;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public CommunityInfo id(final String id) {
        setId(id);

        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public CommunityInfo name(final String name) {
        setName(name);

        return this;
    }

    public List<Program> getPrograms() {
        return programs;
    }

    public void setPrograms(final List<Program> programs) {
        this.programs = programs;
    }

    public CommunityInfo programs(Program... programs) {
        setPrograms(Arrays.asList(programs));

        return this;
    }

    public ScheduleItem[] getScheduleByDate(final long datetime) {
        final Set<ScheduleItem> res = new TreeSet<>(new Comparator<ScheduleItem>() {
            private final DateFormat df = new SimpleDateFormat("HH:mm");

            @Override
            public int compare(ScheduleItem o1, ScheduleItem o2) {
                try {
                    return (int)(df.parse(o1.getHour()).getTime() - df.parse(o2.getHour()).getTime());
                } catch (Throwable t) {
                    return 0;
                }
            }
        });

        EDay day = EDay.getTodayOfWeek(datetime);
        String date = new SimpleDateFormat("dd-MM-YYYY").format(datetime);
        for (final Program p: this.programs) {
            if (p.getDays() != null) {
                if (EDay.in(p.getDays(), day)) {
                    res.addAll(p.getScheduleItems());
                }
            }

            if (p.getDates() != null) {
                for (final String d: p.getDates()) {
                    if (d.equals(date)) {
                        res.addAll(p.getScheduleItems());
                    }
                }
            }
        }

        return res.toArray(new ScheduleItem[res.size()]);
    }

}