/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import me.yoram.android.zmani.adapters.CommunitySpecAdapter;
import me.yoram.android.zmani.dialogs.NewCommunityDialog;
import me.yoram.android.zmani.persistence.PersistenceLayer;
import me.yoram.android.zmani.persistence.CommunitySpec;

import java.util.Properties;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 14/03/18
 */
public class CommunityActivity extends AppCompatActivity {
    private class RefreshData implements Runnable {
        @Override
        public void run() {
            PersistenceLayer db = App.getInstance().getDatabase();
            final CommunitySpec[] communitySpecs = db.communitySpecDao().getAll();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final ListView list = findViewById(R.id.community_list);
                    list.setAdapter(new CommunitySpecAdapter(CommunityActivity.this, communitySpecs));
                    list.invalidate();
                }
            });
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_community);

        final FloatingActionButton addBtn = findViewById(R.id.activity_community_add);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new NewCommunityDialog().onSelected(
                        new NewCommunityDialog.OnSelectedCommunityListener() {
                            @Override
                            public void onSelected(final CommunitySpec spec) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        PersistenceLayer db = App.getInstance().getDatabase();
                                        db.communitySpecDao().insert(spec);
                                        new Thread(new RefreshData()).start();
                                    }
                                }).start();
                            }
                        });

                newFragment.show(CommunityActivity.this.getFragmentManager(), "newcommunity");

            }
        });

        final ListView list = findViewById(R.id.community_list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final CommunitySpec spec = (CommunitySpec) ((ListView) parent).getAdapter().getItem(position);

                final Properties props = PersistenceLayer.getProperties(view);
                props.setProperty(PersistenceLayer.DEFAULT_COMMUNITY, spec.getId());
                PersistenceLayer.setProperties(props, view);

                Intent intent = new Intent(CommunityActivity.this, TimeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        new Thread(new RefreshData()).start();
    }
}
