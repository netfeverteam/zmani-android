/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import me.yoram.android.zmani.R;
import me.yoram.android.zmani.persistence.CommunitySpec;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 15/03/18
 */
public class CommunitySpecAdapter extends AbstractAdapter<CommunitySpec> {
    public CommunitySpecAdapter(final Context context, final CommunitySpec... dataset) {
        super(context, dataset);

        comparator(new Comparator<CommunitySpec>() {
            @Override
            public int compare(CommunitySpec o1, CommunitySpec o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        Set<CommunitySpec> set = new TreeSet<>(getComparator());
        set.addAll(Arrays.asList(dataset));
        dataset(set.toArray(new CommunitySpec[set.size()]));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View res = (convertView == null) ?
                LayoutInflater.from(getContext()).inflate(R.layout.dialog_find_community_list_item, null) :
                convertView;

        if (res != null) {
            CommunitySpec info = (CommunitySpec) getItem(position);

            ((TextView)res.findViewById(R.id.dialog_find_community_list_item_name)).setText(info.getName());

            if (info.getB64Icon() != null) {
                ((ImageView)res
                        .findViewById(R.id.dialog_find_community_list_item_icon))
                        .setImageBitmap(
                                BitmapFactory.decodeStream(
                                        new ByteArrayInputStream(Base64.decode(info.getB64Icon(), Base64.DEFAULT))));
            }
        }

        return res;
    }

    @Override
    public CommunitySpecAdapter comparator(Comparator<CommunitySpec> comparator) {
        super.comparator(comparator);

        return this;
    }
}
