/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.utils.dto;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 11/03/18
 */
public enum EDay {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY;

    public static EDay getTodayOfWeek(final long datetime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(datetime));
        return EDay.values()[calendar.get(Calendar.DAY_OF_WEEK) - 1];
    }

    public static boolean in(EDay[] days, EDay toFind) {
        if (days == null || toFind == null) {
            return false;
        }

        for (final EDay d: days) {
            if (d == toFind) {
                return true;
            }
        }

        return false;
    }

    public static boolean in(Set<EDay> days, EDay toFind) {
        return in(days.toArray(new EDay[days.size()]), toFind);
    }
}
