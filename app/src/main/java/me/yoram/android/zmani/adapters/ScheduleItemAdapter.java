/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import me.yoram.android.zmani.R;
import me.yoram.android.zmani.utils.dto.ScheduleItem;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 11/03/18
 */
public class ScheduleItemAdapter extends RecyclerView.Adapter<ScheduleItemAdapter.ViewHolder> {
    private ScheduleItem[] mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout item;

        public ViewHolder(RelativeLayout item) {
            super(item);
            this.item = item;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ScheduleItemAdapter(ScheduleItem[] myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ScheduleItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        RelativeLayout itemLayout = (RelativeLayout) LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.time_list_item, parent, false);

        ViewHolder vh = new ViewHolder(itemLayout);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        ((TextView)holder.item.findViewById(R.id.timedesc)).setText(mDataset[position].getHour() + "  " + mDataset[position].getTitle());
        ((TextView)holder.item.findViewById(R.id.description)).setText(mDataset[position].getShortDescription());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}