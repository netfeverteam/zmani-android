/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import me.yoram.android.zmani.App;

import java.io.*;
import java.util.Date;
import java.util.Properties;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 15/03/18
 */
@Database(entities = {CommunitySpec.class}, version = 2, exportSchema = false)
public abstract  class PersistenceLayer extends RoomDatabase {
    public static final String DEFAULT_COMMUNITY = "community.default";

    public static synchronized Properties getProperties(View v)  {
        final Properties res = new Properties();
        try {
            final File f = new File(App.getInstance().getApplicationContext().getFilesDir(), "config.properties");

            if (f.exists()) {
                InputStream in = App.getInstance().getApplicationContext().openFileInput("config.properties");

                try {
                    res.load(in);
                } finally {
                    in.close();
                }
            }
        } catch (Throwable t) {
            Snackbar.make(v, t.getMessage(), 1000);
            t.printStackTrace();
        }

        return res;
    }

    public static synchronized void setProperties(final Properties props, final View v)  {
        try {
            OutputStreamWriter out  = new OutputStreamWriter(
                    App.getInstance().openFileOutput("config.properties", Context.MODE_PRIVATE));

            try {
                props.store(out, new Date().toString());
            } finally {
                out.close();
            }
        } catch (Throwable t) {
            Snackbar.make(v, t.getMessage(), 1000);
            t.printStackTrace();
        }
    }

    public abstract CommunitySpecDao communitySpecDao();
}
