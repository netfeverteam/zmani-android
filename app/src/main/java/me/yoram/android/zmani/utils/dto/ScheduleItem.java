/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.utils.dto;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 11/03/18
 */
public class ScheduleItem {
    private String hour;
    private String title;
    private String shortDescription;
    private ScheduleDetails details;

    public String getHour() {
        return hour;
    }

    public void setHour(final String hour) {
        this.hour = hour;
    }

    public ScheduleItem hour(final String hour) {
        setHour(hour);

        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public ScheduleItem title(final String title) {
        setTitle(title);

        return this;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(final String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public ScheduleItem shortDescription(final String shortDescription) {
        setShortDescription(shortDescription);

        return this;
    }

    public ScheduleDetails getDetails() {
        return details;
    }

    public void setDetails(final ScheduleDetails details) {
        this.details = details;
    }

    public ScheduleItem details(final ScheduleDetails details) {
        setDetails(details);

        return this;
    }
}
