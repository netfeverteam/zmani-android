/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import me.yoram.android.zmani.R;
import me.yoram.android.zmani.adapters.CommunitySpecAdapter;
import me.yoram.android.zmani.utils.backend.BackendFactory;
import me.yoram.android.zmani.persistence.CommunitySpec;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 15/03/18
 */
public class NewCommunityDialog extends DialogFragment {
    public interface OnSelectedCommunityListener {
        void onSelected(CommunitySpec spec);
    }

    private OnSelectedCommunityListener onSelected;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_find_community, null);

        ListView list = v.findViewById(R.id.dialog_find_community_list);
        final CommunitySpecAdapter adapter = new CommunitySpecAdapter(
                v.getContext(), BackendFactory.getBackend().getCommunitySpecs());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (NewCommunityDialog.this.onSelected != null) {
                    final CommunitySpec spec = (CommunitySpec)((ListView)parent).getAdapter().getItem(position);
                    NewCommunityDialog.this.onSelected.onSelected(spec);
                }

                dismiss();
            }
        });

        EditText text = v.findViewById(R.id.dialog_find_community_filter);
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        builder.setView(v);

        return builder.create();
    }

    public NewCommunityDialog onSelected(OnSelectedCommunityListener onSelected) {
        this.onSelected = onSelected;

        return this;
    }
}
