/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.utils.backend;

import android.util.Base64;
import me.yoram.android.zmani.App;
import me.yoram.android.zmani.R;
import me.yoram.android.zmani.persistence.CommunitySpec;
import me.yoram.android.zmani.utils.dto.*;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.util.Arrays;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 11/03/18
 */
public class DummyBackend implements IBackend {
    private CommunityInfo getAmatzia() {
        return new CommunityInfo()
                .id("Amatzia")
                .name("Amatzia")
                .programs(
                        new Program()
                                .id("Tfila Weekly")
                                .classifier("Weekly")
                                .language("English")
                                .locale("en")
                                .days(EDay.SUNDAY, EDay.MONDAY, EDay.TUESDAY, EDay.WEDNESDAY, EDay.THURSDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("05:45")
                                                .title("Shacharit")
                                                .shortDescription("Morning Prayer"),
                                        new ScheduleItem()
                                                .hour("06:45")
                                                .title("Shacharit")
                                                .shortDescription("Morning Prayer"),
                                        new ScheduleItem()
                                                .hour("17:30")
                                                .title("Minha")
                                                .shortDescription("Afternoon Prayer"),
                                        new ScheduleItem()
                                                .hour("18:15")
                                                .title("Arvit")
                                                .shortDescription("Evening Prayer"),
                                        new ScheduleItem()
                                                .hour("19:00")
                                                .title("Arvit")
                                                .shortDescription("Evening Prayer"),
                                        new ScheduleItem()
                                                .hour("21:00")
                                                .title("Arvit")
                                                .shortDescription("Evening Prayer")),
                        new Program()
                                .id("Tfila Friday")
                                .classifier("Ashkenazi")
                                .language("English")
                                .locale("en")
                                .days(EDay.FRIDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("05:45")
                                                .title("Shacharit")
                                                .shortDescription("Morning Prayer"),
                                        new ScheduleItem()
                                                .hour("06:45")
                                                .title("Shacharit")
                                                .shortDescription("Morning Prayer"),
                                        new ScheduleItem()
                                                .hour("13:30")
                                                .title("Minha")
                                                .shortDescription("Afternoon Prayer"),
                                        new ScheduleItem()
                                                .hour("17:20")
                                                .title("Minha")
                                                .shortDescription("Afternoon Prayer"),
                                        new ScheduleItem()
                                                .hour("17:40")
                                                .title("Arvit")
                                                .shortDescription("Evening Prayer")),
                        new Program()
                                .id("Tfila Friday")
                                .classifier("Sephardi")
                                .language("English")
                                .locale("en")
                                .days(EDay.FRIDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("05:45")
                                                .title("Shacharit")
                                                .shortDescription("Morning Prayer"),
                                        new ScheduleItem()
                                                .hour("06:45")
                                                .title("Shacharit")
                                                .shortDescription("Morning Prayer"),
                                        new ScheduleItem()
                                                .hour("13:30")
                                                .title("Minha")
                                                .shortDescription("Afternoon Prayer"),
                                        new ScheduleItem()
                                                .hour("17:40")
                                                .title("Arvit")
                                                .shortDescription("Evening Prayer")),

                        new Program()
                                .id("Tfila Shabbat")
                                .classifier("Ashkenazi")
                                .language("English")
                                .locale("en")
                                .days(EDay.SATURDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("08:00")
                                                .title("Shacharit")
                                                .shortDescription("Morning Prayer"),
                                        new ScheduleItem()
                                                .hour("15:30")
                                                .title("Minha")
                                                .shortDescription("Afternoon Prayer"),
                                        new ScheduleItem()
                                                .hour("16:40")
                                                .title("Minha")
                                                .shortDescription("Afternoon Prayer"),
                                        new ScheduleItem()
                                                .hour("18:20")
                                                .title("Arvit")
                                                .shortDescription("Evening Prayer")),
                        new Program()
                                .id("Tfila Shabbat")
                                .classifier("Sephardi")
                                .language("English")
                                .locale("en")
                                .days(EDay.SATURDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("07:35")
                                                .title("Shacharit")
                                                .shortDescription("Morning Prayer"),
                                        new ScheduleItem()
                                                .hour("15:30")
                                                .title("Minha")
                                                .shortDescription("Afternoon Prayer"),
                                        new ScheduleItem()
                                                .hour("16:40")
                                                .title("Minha")
                                                .shortDescription("Afternoon Prayer"),
                                        new ScheduleItem()
                                                .hour("18:20")
                                                .title("Arvit")
                                                .shortDescription("Evening Prayer")),
                        new Program()
                                .id("shiur-123")
                                .classifier("Ashkenazi")
                                .language("English")
                                .locale("en")
                                .dates("17-03-2018")
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("14:00")
                                                .title("Shiur with Rav Aritov")
                                                .shortDescription("Parasha Vayikra"),
                                        new ScheduleItem()
                                                .hour("14:45")
                                                .title("Shiur with Rav Goldberg")
                                                .shortDescription("Gmara 22b")));

    }

    private CommunityInfo getBneDkalim() {
        return new CommunityInfo()
                .id("Bne Dkalim")
                .name("Bne Dkalim")
                .programs(
                        new Program()
                                .id("Tfila Weekly")
                                .classifier("Weekly")
                                .language("עברית")
                                .locale("he")
                                .days(EDay.SUNDAY, EDay.MONDAY, EDay.TUESDAY, EDay.WEDNESDAY, EDay.THURSDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("05:45")
                                                .title("שחרית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("06:45")
                                                .title("שחרית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("17:30")
                                                .title("מנחה")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("18:15")
                                                .title("ערבית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("19:00")
                                                .title("ערבית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("21:00")
                                                .title("ערבית")
                                                .shortDescription("שים תיאור כאן")),
                        new Program()
                                .id("Tfila Friday")
                                .classifier("Ashkenazi")
                                .language("עברית")
                                .locale("he")
                                .days(EDay.FRIDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("05:45")
                                                .title("שחרית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("06:45")
                                                .title("שחרית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("13:30")
                                                .title("מנחה")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("17:20")
                                                .title("מנחה")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("17:40")
                                                .title("ערבית")
                                                .shortDescription("שים תיאור כאן")),
                        new Program()
                                .id("Tfila Friday")
                                .classifier("Sephardi")
                                .language("עברית")
                                .locale("he")
                                .days(EDay.FRIDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("05:45")
                                                .title("שחרית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("06:45")
                                                .title("שחרית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("13:30")
                                                .title("מנחה")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("17:40")
                                                .title("ערבית")
                                                .shortDescription("שים תיאור כאן")),

                        new Program()
                                .id("Tfila Shabbat")
                                .classifier("Ashkenazi")
                                .language("עברית")
                                .locale("he")
                                .days(EDay.SATURDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("08:00")
                                                .title("שחרית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("15:30")
                                                .title("Minha")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("16:40")
                                                .title("מנחה")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("18:20")
                                                .title("ערבית")
                                                .shortDescription("שים תיאור כאן")),
                        new Program()
                                .id("Tfila Shabbat")
                                .classifier("Sephardi")
                                .language("עברית")
                                .locale("he")
                                .days(EDay.SATURDAY)
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("07:35")
                                                .title("שחרית")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("15:30")
                                                .title("מנחה")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("16:40")
                                                .title("מנחה")
                                                .shortDescription("שים תיאור כאן"),
                                        new ScheduleItem()
                                                .hour("18:20")
                                                .title("ערבית")
                                                .shortDescription("שים תיאור כאן")),
                        new Program()
                                .id("shiur-123")
                                .classifier("Ashkenazi")
                                .language("עברית")
                                .locale("he")
                                .dates("17-03-2018")
                                .scheduleItems(
                                        new ScheduleItem()
                                                .hour("14:00")
                                                .title("שיעור עם הרב אחיטוב")
                                                .shortDescription("פרשה ויקרא"),
                                        new ScheduleItem()
                                                .hour("14:45")
                                                .title("שיעור עם הרב גולדברג")
                                                .shortDescription("גמרא מסכת תענית")));
    }

    @Override
    public CommunitySpec[] getCommunitySpecs() {
        String b64;
        final InputStream in = App.getInstance().getApplicationContext().getResources().openRawResource(R.raw.bndkalim);

        try {
            b64 = Base64.encodeToString(IOUtils.toByteArray(in), Base64.DEFAULT);
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        } finally {
            IOUtils.closeQuietly(in);
        }

        return new CommunitySpec[] {
                new CommunitySpec().id("Amatzia").name("Amatzia"),
                new CommunitySpec().id("Bne Dkalim").name("בני דקלים").b64Icon(b64)
        };
    }

    @Override
    public String[] getMyCommunityIds() {
        return new String[] {"בני דקלים", "Amatzia"};
    }

    @Override
    public CommunityInfo[] getMyCommunities() {
        return Arrays.asList(getAmatzia(), getBneDkalim()).toArray(new CommunityInfo[2]);
    }

    @Override
    public CommunityInfo getCommunityInfo(String id) {
        if ("Amatzia".equals(id)) {
            return getAmatzia();
        } else {
            return getBneDkalim();
        }
    }
}
