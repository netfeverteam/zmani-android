/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.persistence;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import me.yoram.android.zmani.adapters.AbstractAdapter;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 15/03/18
 */
@Entity
public class CommunitySpec implements AbstractAdapter.FilteredObject {
    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "b64Icon")
    private String b64Icon;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public CommunitySpec id(final String id) {
        setId(id);

        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public CommunitySpec name(final String name) {
        setName(name);

        return this;
    }

    public String getB64Icon() {
        return b64Icon;
    }

    public void setB64Icon(String b64Icon) {
        this.b64Icon = b64Icon;
    }

    public CommunitySpec b64Icon(String b64Icon) {
        setB64Icon(b64Icon);

        return this;
    }

    @Override
    public boolean contains(String s) {
        return name.toLowerCase().contains(s);
    }
}
