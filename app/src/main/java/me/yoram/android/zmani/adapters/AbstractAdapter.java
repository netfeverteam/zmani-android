/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.adapters;

import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.*;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 15/03/18
 */
public abstract class AbstractAdapter<T> extends BaseAdapter implements Filterable {
    public interface FilteredObject {
        boolean contains(String s);
    }

    private class AdapterFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final FilterResults res = new FilterResults();

            final String toFind = constraint.toString().trim().toLowerCase();

            if (toFind.length() == 0) {
                res.count = savedDataset.length;
                res.values = savedDataset;
            } else {
                final Collection<T> set = comparator == null ? new ArrayList<T>() : new TreeSet<>(comparator);

                for (T o: savedDataset) {
                    if (o instanceof FilteredObject) {
                        if (((FilteredObject)o).contains(toFind)) {
                            set.add(o);
                        }
                    } else if (o.toString().toLowerCase().contains(toFind)){
                        set.add(o);
                    }
                }

                res.count = set.size();
                res.values = set.toArray();
            }

            return res;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataset = (T[]) results.values;
            notifyDataSetChanged();
        }
    }

    private final Context context;
    private T[] dataset;
    private T[] savedDataset;
    private final Filter filter = new AdapterFilter();
    private Comparator<T> comparator;

    @SafeVarargs
    public AbstractAdapter(final Context context, final T... dataset) {
        super();
        this.context = context;
        this.dataset = dataset;
        this.savedDataset = dataset;
    }

    public Context getContext() {
        return context;
    }

    public T[] getDataset() {
        return dataset;
    }

    public int getCount() {
        return dataset.length;
    }

    public Object getItem(final int position) {
        return dataset[position];
    }

    public long getItemId(final int position) {
        return position;
    }

    public Comparator<T> getComparator() {
        return comparator;
    }

    protected AbstractAdapter<T> dataset(T[] dataset) {
        this.dataset = dataset;
        this.savedDataset = dataset;
        notifyDataSetInvalidated();

        return this;
    }

    protected AbstractAdapter<T> comparator(Comparator<T> comparator) {
        this.comparator = comparator;

        return this;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
}
