/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.utils.dto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 14/03/18
 */
public class Program {
    /**
     * Identify the unique program. Many version of the same program can be specified if it has the same id.
     */
    private String id = UUID.randomUUID().toString();

    /**
     * Each program can be in a language
     */
    private String language = "English";

    /**
     * Classifier is an extra classification, for example "boys or girl", the age, etc...
     */
    private String classifier;

    /**
     * What day this program run on. This can be <code>null</code> or empty if {@link #dates} is filled.
     */
    private Set<EDay> days;

    /**
     * What dates this program run on.
     *
     * This is a list of string formatted dd-mm-yyyy. If this list is <code>null</code> or empty then the {@link #days}
     * must be filled. If it's filled then it will be use in addition to the {@link #days} if filled.
     */
    private Set<String> dates;

    /**
     * The schedule
     */
    private Set<ScheduleItem> scheduleItems;

    /**
     * Used for debug
     */
    private String locale;


    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Program id(final String id) {
        setId(id);

        return this;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public Program language(final String language) {
        setLanguage(language);

        return this;
    }

    public String getClassifier() {
        return classifier;
    }

    public void setClassifier(final String classifier) {
        this.classifier = classifier;
    }

    public Program classifier(final String classifier) {
        setClassifier(classifier);

        return this;
    }

    public Set<ScheduleItem> getScheduleItems() {
        return scheduleItems;
    }

    public void setScheduleItems(final Set<ScheduleItem> scheduleItems) {
        this.scheduleItems = scheduleItems;
    }

    public Program scheduleItems(final ScheduleItem... scheduleItems) {
        setScheduleItems(new HashSet<>(Arrays.asList(scheduleItems)));

        return this;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(final String locale) {
        this.locale = locale;
    }

    public Program locale(final String locale) {
        setLocale(locale);

        return this;
    }

    public Set<EDay> getDays() {
        return days;
    }

    public void setDays(final Set<EDay> days) {
        this.days = days;
    }

    public Program days(EDay... days) {
        setDays(new HashSet<EDay>(Arrays.asList(days)));

        return this;
    }

    public Set<String> getDates() {
        return dates;
    }

    public void setDates(final Set<String> dates) {
        this.dates = dates;
    }

    public Program dates(String... dates) {
        setDates(new HashSet<String>(Arrays.asList(dates)));

        return this;
    }
}
