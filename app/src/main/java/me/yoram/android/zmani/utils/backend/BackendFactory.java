/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.utils.backend;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 11/03/18
 */
public class BackendFactory {
    public static IBackend getBackend() {
        return new DummyBackend();
    }
}
