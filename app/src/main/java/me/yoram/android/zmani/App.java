/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Intent;
import me.yoram.android.zmani.persistence.PersistenceLayer;
import me.yoram.android.zmani.utils.DebugTools;

import java.util.Properties;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 15/03/18
 */
public class App extends Application {
    private static App instance;
    private PersistenceLayer database;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        database = Room
                .databaseBuilder(getApplicationContext(), PersistenceLayer.class, "zmani-persistence")
                .fallbackToDestructiveMigration()
                .build();
    }

    public PersistenceLayer getDatabase() {
        return database;
    }
}
