package me.yoram.android.zmani;

import android.app.*;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.PopupMenu;
import android.widget.TextView;
import me.yoram.android.zmani.adapters.ScheduleItemAdapter;
import me.yoram.android.zmani.fragments.DatePickerFragment;
import me.yoram.android.zmani.persistence.PersistenceLayer;
import me.yoram.android.zmani.utils.backend.BackendFactory;
import me.yoram.android.zmani.utils.dto.CommunityInfo;
import me.yoram.android.zmani.utils.dto.ScheduleItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class TimeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DatePickerDialog.OnDateSetListener {
    private long currentDateTime = System.currentTimeMillis();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currentDateTime = savedInstanceState == null ?
                System.currentTimeMillis() :
                savedInstanceState.getLong("currentDateTime", System.currentTimeMillis());

        final Properties props = PersistenceLayer.getProperties(null);

        boolean hasDefaultCommunity = !props.containsKey(PersistenceLayer.DEFAULT_COMMUNITY);

        if (hasDefaultCommunity) {
            Intent intent = new Intent(this, CommunityActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu menu = new PopupMenu(TimeActivity.this, view);

                for (String s: BackendFactory.getBackend().getMyCommunityIds()) {
                    menu.getMenu().add(s);
                }
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String id = item.getTitle().toString();
                        final CommunityInfo info = BackendFactory.getBackend().getCommunityInfo(id);
                        ScheduleItem[] items = info.getScheduleByDate(currentDateTime);
                        ScheduleItemAdapter mAdapter = new ScheduleItemAdapter(items);

                        RecyclerView mRecyclerView = findViewById(R.id.zmanlist);
                        mRecyclerView.setAdapter(mAdapter);
                        mRecyclerView.invalidate();

                        /*
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (info.getLo() != null) {
                                    Configuration configuration = getResources().getConfiguration();
                                    configuration.setLayoutDirection(new Locale(info.getLocaleOverride()));
                                    getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
                                }

                            }
                        });
                        */

                        return true;
                    }
                });
                menu.show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        RecyclerView mRecyclerView = findViewById(R.id.zmanlist);
        TextView zmanDateHeader = findViewById(R.id.zman_date_header);
        zmanDateHeader.setText(new SimpleDateFormat("EEE, MMM dd").format(new Date(currentDateTime)));

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        CommunityInfo info = BackendFactory.getBackend().getCommunityInfo(PersistenceLayer.getProperties(null).getProperty(PersistenceLayer.DEFAULT_COMMUNITY));
        ScheduleItem[] items = info.getScheduleByDate(currentDateTime);

        ScheduleItemAdapter mAdapter = new ScheduleItemAdapter(items);
        mRecyclerView.setAdapter(mAdapter);

        /*
        if (info.getLocaleOverride() != null) {
            Configuration configuration = getResources().getConfiguration();
            configuration.setLayoutDirection(new Locale(info.getLocaleOverride()));
            getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
        }
        */
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putLong("currentDateTime", currentDateTime);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("currentDateTime", currentDateTime);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
        } else if (id == R.id.nav_communities) {
            Intent intent = new Intent(this, CommunityActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onDateClick(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setDatePickerFragment(this);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        try {
            DateFormat fromSource = new SimpleDateFormat("d/M/yyyy");
            Date dtSource = fromSource.parse(dayOfMonth + "/" + (month + 1) + "/" + year);
            currentDateTime = dtSource.getTime();

            TextView zmanDateHeader = findViewById(R.id.zman_date_header);
            zmanDateHeader.setText(new SimpleDateFormat("EEE, MMM dd").format(dtSource));

            ScheduleItem[] items = BackendFactory.getBackend().getCommunityInfo("111").getScheduleByDate(currentDateTime);
            ScheduleItemAdapter mAdapter = new ScheduleItemAdapter(items);

            RecyclerView mRecyclerView = findViewById(R.id.zmanlist);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.invalidate();
        } catch (Throwable t) {

        }

    }
}
