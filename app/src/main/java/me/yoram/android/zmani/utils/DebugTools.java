/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.utils;

import me.yoram.android.zmani.App;
import me.yoram.android.zmani.persistence.PersistenceLayer;

import java.io.File;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 14/03/18
 */
public class DebugTools {
    public static void _wait(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {

        }
    }

    public static void deleteProperties() {
        try {
            final File f = new File(App.getInstance().getApplicationContext().getFilesDir(), "config.properties");

            if (f.exists()) {
                f.delete();
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void deleteCommunities() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                App.getInstance().getDatabase().communitySpecDao().deleteAllCommunities();
            }
        }).start();
    }
}
