/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.persistence;

import android.arch.persistence.room.*;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 15/03/18
 */
@Dao
public interface CommunitySpecDao {
    @Query("SELECT * FROM communitySpec")
    CommunitySpec[] getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CommunitySpec config);

    @Delete
    void delete(CommunitySpec config);

    @Query("Delete FROM communitySpec")
    void deleteAllCommunities();
}
