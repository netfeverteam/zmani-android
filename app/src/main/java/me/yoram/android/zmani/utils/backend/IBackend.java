/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.android.zmani.utils.backend;

import me.yoram.android.zmani.utils.dto.CommunityInfo;
import me.yoram.android.zmani.persistence.CommunitySpec;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 11/03/18
 */
public interface IBackend {
    String[] getMyCommunityIds();
    CommunitySpec[] getCommunitySpecs();
    CommunityInfo[] getMyCommunities();
    CommunityInfo getCommunityInfo(String id);
}
